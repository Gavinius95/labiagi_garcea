#include <ros/ros.h>
#include "sensor_msgs/LaserScan.h"
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <stdio.h>

#define PARAM_VISUALE 400    		//questo paramentro definisce il range di "visuale" frontale.

sensor_msgs::LaserScan laser;
nav_msgs::Odometry odom;
geometry_msgs::Twist vel;

//funzione che calcola il minimo su un array e ritorna il valore
float min_array(float array[], int len)
{
	float min = array[0];
	for(int k=0; k < len; k++)
	{
		if(array[k] < min )
		{
			min = array[k];
		}
	}
	return min;
} 

//funzione callback per il LaserScan
//la funzione fa solo un'assegnazione
void callback_scan(const sensor_msgs::LaserScan::ConstPtr& msg)
{
	//ROS_INFO("Lettura dei valori del LaserScan...");
	laser = *msg;	
}

//funzione callback per Odometry
//la funzione fa solo un'assegnazione
void callback_odom(const nav_msgs::Odometry::ConstPtr& msg)
{
	//ROS_INFO("Lettura dei valori di Odometry...");
	odom = *msg;
}

//main
int main(int argc , char* argv [])
{
	//ROS_INFO("Inizio del main");
	ros::init(argc,argv,"Collision_avoidance"); 					// lancia il nodo nell'ambiente roscore
	ros::NodeHandle n;  											// oggetto su cui si invoca: advertise(), subscribe() 
	
	ros::Publisher vel_pub;											// publisher per il cmd_vel
	vel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);	// topic dove si pubblica la velocità
	
	// variabile per cmd_vel
	vel.linear.x = 0.0;		  										// inizializzano a 0.0 tutti i suoi campi del messaggio
	vel.linear.y = 0.0;
	vel.linear.z = 0.0;
	vel.angular.x = 0.0;
	vel.angular.y = 0.0;
	vel.angular.z = 0.0;
	
	ros::Subscriber scanner_sub = n.subscribe("/base_scan", 1000, callback_scan); 		// legge da /base_scan messaggi del tipo : sensor_msgs/LaserScan
	
	ros::Subscriber odometry_sub = n.subscribe("/odom", 1000, callback_odom);			// legge da /odom messaggi del tipo : nav_msgs/Odometry
	
	ros::Rate loop_rate(10);												//5hz

	bool check = true;													// controlla se ha preso la decisione 
	while(ros::ok())
	{
		float odom_x = odom.pose.pose.position.x;
		float odom_y = odom.pose.pose.position.y;
		ROS_INFO("Posizione x: %f; y: %f\n", odom_x, odom_y);
		
		int len = laser.ranges.size();									// misuro la lunghezza dell'array che mantieni i valori del laser
		
		//algorimto decisore//
			if (len > 0)												// condizione di controllo sui dati ricevuti
			{
				float destra[len/2];									//dividiamo i dati del laser in due array (parte destra)
				float sinistra[len - len/2];							//dividiamo i dati del laser in due array (parte sinistra)
				float centro[len - 2*PARAM_VISUALE];					//dichiariamo una zona di "visuale" centrale
				
				for(int i = 0; i < len; i++)							// il for serve per copiare i dati nei nuovi array
				{
					if(i < len/2) destra[i] = laser.ranges[i];			// destra[0,len/2-1]
					else sinistra[i-len/2] = laser.ranges[i];			// sinistra[len/2, len]
					if(i >= PARAM_VISUALE ||  i < len-PARAM_VISUALE) centro[i - PARAM_VISUALE] = laser.ranges[i]; //centro[PARAM_VISUALE, len-PARAM_VISUALE-1]
					//ROS_INFO("lunghezza destra %d", destra.size());
					//ROS_INFO("lunghezza totale %d", len);
				}
				
				if(min_array(centro, len - 2*PARAM_VISUALE) > 0.8)		//controlla se c'è un ostacolo davanti
				{
					vel.linear.x = 1;
					vel.angular.z = 0.0;
					check=true;											//al prossimo ostacolo va presa una decisione	
				}
				else {
					if(check == true){									
						if(min_array(destra,len/2) > min_array(sinistra,len - len/2)) vel.angular.z=-0.5;	//gira a destra
						else vel.angular.z=0.5;							//gira sinistra
						check=false;									//decisione presa, non dobbiamo cambiare fino al prossimo ostacolo
					}
					vel.linear.x = 0.0;									// quando gira voglio velocità lineare zero
				}
			
			vel_pub.publish(vel);										//pubblica la velocità
			
			}
		
	    ros::spinOnce();
	    loop_rate.sleep();
        
	}
	
	return 0;
	
}






